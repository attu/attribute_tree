from attribute_tree import attribute_tree, walk


def test_walking_empty_attribute_tree_yields_nothing():
    assert tuple(walk(attribute_tree())) == tuple()


def test_walking_non_nested_attribute_tree_yields_flat_paths():
    tree = attribute_tree(x=1, y=2)
    assert set(walk(tree)) == {(('x',), 1), (('y',), 2)}


def test_walking_nested_attribute_tree_yields_flat_paths():
    tree = attribute_tree(x=1, y=2)
    tree.z.a = 3
    assert set(walk(tree)) == {(('x',), 1), (('y',), 2), (('z', 'a'), 3)}


def test_walking_nested_lists_of_attribute_trees_yields_flat_paths():
    tree = attribute_tree(x=1, y=2)
    tree.z.a = [attribute_tree(b=3)]
    assert set(walk(tree)) == {(('x',), 1), (('y',), 2), (('z', 'a', 0, 'b'), 3)}


def test_walking_attribute_tree_with_nested_tuples():
    tree = attribute_tree()
    tree.x = (attribute_tree(y=1), attribute_tree(z=2), list(), tuple(), dict())
    assert set(walk(tree)) == {(('x', 0, 'y'), 1), (('x', 1, 'z'), 2)}
