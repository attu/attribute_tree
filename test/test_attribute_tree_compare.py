from attribute_tree import attribute_tree


def test_empty_attribute_tree_compares_equal():
    lhs = attribute_tree()
    rhs = attribute_tree()
    assert id(lhs) != id(rhs)
    assert lhs == rhs
    assert not lhs != rhs


def test_non_empty_same_attribute_trees_compares_equal():
    lhs = attribute_tree(x=1, y=2)
    rhs = attribute_tree(x=1, y=2)
    assert id(lhs) != id(rhs)
    assert lhs == rhs
    assert not lhs != rhs


def test_different_attribute_trees_compares_non_equal():
    lhs = attribute_tree(x=1, y=2)
    rhs = attribute_tree(x=1, z=3)
    assert id(lhs) != id(rhs)
    assert not lhs == rhs
    assert lhs != rhs


def test_can_compare_nested_attribute_trees():
    lhs = attribute_tree(x=1, y=2)
    rhs = attribute_tree(x=1, y=2)
    assert id(lhs) != id(rhs)
    assert lhs == rhs

    lhs.z.a = [attribute_tree(b=3)]
    rhs.z.a = [attribute_tree(b=4)]
    assert lhs != rhs

    lhs.z.a = 3
    rhs.z.a = 3
    assert lhs == rhs


def test_attribute_tree_that_compares_less_is_a_subset():
    lhs = attribute_tree(x=1, y=2)
    rhs = attribute_tree(x=1)
    assert lhs > rhs
    assert rhs < lhs

    rhs.x = 3
    assert not lhs > rhs
    assert not rhs < lhs
