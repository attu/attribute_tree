from attribute_tree import attribute_tree, keys, items, values


def test_can_getitems_from_attribute_tree():
    tree = attribute_tree(lorem='ipsum')
    assert tree['lorem'] == 'ipsum'


def test_can_setitems_for_attribure_tree():
    tree = attribute_tree()
    tree['lorem'] = 'ipsum'

    assert tree.lorem == 'ipsum'


def test_can_get_attribute_tree_dict_methods():
    tree = attribute_tree(lorem='ipsum', dolor='sit')

    assert set(keys(tree)) == {'lorem', 'dolor'}
    assert set(items(tree)) == {('lorem', 'ipsum'), ('dolor', 'sit')}
    assert set(values(tree)) == {'ipsum', 'sit'}
