from attribute_tree import attribute_tree


def test_empty_attribute_tree_has_empty_vars():
    tree = attribute_tree()
    assert vars(tree) == dict()


def test_can_set_attributes_via_ctor():
    tree = attribute_tree(x=1, y=2)
    assert vars(tree) == dict(x=1, y=2)
    assert tree.x == 1
    assert tree.y == 2


def test_can_setitem():
    tree = attribute_tree(x=1, y=2)
    tree.z = 3
    assert vars(tree) == dict(x=1, y=2, z=3)
    assert tree.z == 3
    tree.x = 4
    assert vars(tree) == dict(x=4, y=2, z=3)
    assert tree.x == 4


def test_can_delitem():
    tree = attribute_tree(x=1, y=2)
    del tree.y
    assert vars(tree) == dict(x=1)


def test_can_assign_nested_attributes():
    tree = attribute_tree(x=1, y=2)
    tree.z.a = 3
    assert tree.z.a == 3
    assert vars(tree.z) == dict(a=3)
    assert vars(tree) == dict(x=1, y=2, z=dict(a=3))


def test_can_set_nested_attributes_in_ctor():
    tree = attribute_tree(
        lorem='ipsum',
        dolor=dict(sit='amet'),
        consectetur=[
            dict(adipiscing='eiusmod'),
            dict(tempor='incididunt')
        ],
        ut=(
            dict(labore='et dolore'),
            dict(magna='aliqua')
        ))

    assert tree.lorem == 'ipsum'
    assert tree.dolor.sit == 'amet'
    assert isinstance(tree.consectetur, list)
    assert tree.consectetur[0].adipiscing == 'eiusmod'
    assert tree.consectetur[1].tempor == 'incididunt'
    assert isinstance(tree.ut, tuple)
    assert tree.ut[0].labore == 'et dolore'
    assert tree.ut[1].magna == 'aliqua'


def test_can_set_nested_dictionaries():
    tree = attribute_tree()
    tree.lorem = dict(ipsum='dolor', sit=dict(amet='consectetur'))

    assert tree.lorem.ipsum == 'dolor'
    assert tree.lorem.sit.amet == 'consectetur'

    tree.adipiscing = [
        dict(eiusmod='tempor', incididunt=dict(ut='et dolore')),
        dict(magna='aliqua')
    ]

    assert isinstance(tree.adipiscing, list)
    assert tree.adipiscing[0].eiusmod == 'tempor'
    assert tree.adipiscing[0].incididunt.ut == 'et dolore'
    assert tree.adipiscing[1].magna == 'aliqua'

    tree.enim = (
        dict(ad='minim', veniam=dict(quis='nostrud')),
        dict(exercitation='ullamco')
    )

    assert isinstance(tree.enim, tuple)
    assert tree.enim[0].ad == 'minim'
    assert tree.enim[0].veniam.quis == 'nostrud'
    assert tree.enim[1].exercitation == 'ullamco'
