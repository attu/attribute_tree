import sys


if sys.version_info[0:2] < (3, 5):
    def isclose(a, b, rel_tol=1e-09, abs_tol=0.0):
        return abs(a - b) <= max(rel_tol * max(abs(a), abs(b)), abs_tol)
else:
    from math import isclose


def test_basic_attribute_setting():
    from attribute_tree import attribute_tree

    # create tree attributes like dict items
    tree = attribute_tree(lorem='ipsum')
    assert tree.lorem == 'ipsum'

    # add new attributes to the root of tree
    tree.dolor = 42
    assert tree.dolor == 42

    # add nested attributes
    tree.consectetur.adipiscing = (-1.0 / 12)
    assert isclose(tree.consectetur.adipiscing, -0.08333333333333333)

    # add list of nested attributes
    tree.elit = [attribute_tree(sed='eiusmod'), attribute_tree(tempor='incididunt')]
    assert tree.elit[0].sed == 'eiusmod'
    assert tree.elit[1].tempor == 'incididunt'


def test_converting_tree_to_dictionaty():
    from attribute_tree import attribute_tree

    tree = attribute_tree(lorem='ipsum')
    tree.dolor.sit = 'amet'

    assert vars(tree) == {'lorem': 'ipsum', 'dolor': {'sit': 'amet'}}


def test_nested_trees():
    from attribute_tree import attribute_tree

    tree = attribute_tree(lorem=dict(ipsum='dolor'))

    assert tree.lorem.ipsum == 'dolor'

    tree.sit = [dict(amet='consectetur')]
    assert tree.sit[0].amet == 'consectetur'
