# CHANGELOG

All notable changes to `attribute_tree` will be documented in this file

---

## [0.1]

### Added
- `class attribute_tree`
- `attribute_tree.__getattr__`, `attribute_tree.__setattr__`, `attribute_tree.__delattr__`
- `attribute_tree.__eq__`, `attribute_tree.__ne__`
- `attribute_tree.__lt__`, `attribute_tree.__gt__`
- `def walk`
- `def keys`, `def items`, `def values`

### Changed

### Fixed

### Removed
