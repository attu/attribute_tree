.. attribute_tree documentation master file, created by
   sphinx-quickstart on Mon Apr 30 22:56:05 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to attribute_tree's documentation!
==========================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   modules
   changes



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
