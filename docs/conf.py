import os
import sys
sys.path.insert(0, os.path.abspath('../'))


def getVersion():
    from attribute_tree import __version__ as version
    return version


release = version = getVersion()
project = u'attribute_tree'
copyright = u'2018, Karol Wozniak'
author = u'Karol Wozniak'

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.doctest',
    'sphinx.ext.intersphinx',
    'sphinx.ext.napoleon',
    'sphinx.ext.viewcode',
]

autodoc_default_flags = [
    'members',
    'private-members',
    'special-members',
    'undoc-members',
    'show-inheritance',
]


def setup(app):
    def autodoc_skip_member(app, what, name, obj, skip, options):
        exclusions = {
            '__weakref__',
            '__doc__',
            '__init__',
            '__module__',
            '__dict__',
        }
        exclude = name in exclusions
        return skip or exclude
    app.connect('autodoc-skip-member', autodoc_skip_member)


templates_path = ['_templates']

source_parsers = {
    '.md': 'recommonmark.parser.CommonMarkParser',
}
source_suffix = ['.rst', '.md']

master_doc = 'index'
language = 'en'
exclude_patterns = []
pygments_style = 'sphinx'
html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']
htmlhelp_basename = 'attribute_treedoc'
latex_elements = {
}
latex_documents = [
    (master_doc, 'attribute_tree.tex', u'attribute_tree Documentation',
     u'Karol Wozniak', 'manual'),
]

man_pages = [
    (master_doc, 'attribute_tree', u'attribute_tree Documentation',
     [author], 1)
]

texinfo_documents = [
    (master_doc, 'attribute_tree', u'attribute_tree Documentation',
     author, 'attribute_tree', 'One line description of project.',
     'Miscellaneous'),
]
intersphinx_mapping = {'https://docs.python.org/': None}
