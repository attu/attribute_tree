[![pipeline status](https://gitlab.com/attu/attribute_tree/badges/master/pipeline.svg)](https://gitlab.com/attu/attribute_tree/commits/master)
[![coverage report](https://gitlab.com/attu/attribute_tree/badges/master/coverage.svg)](https://gitlab.com/attu/attribute_tree/commits/master)

# intro

documentation is automatically generated and hosted on
[gitlab pages](https://attu.gitlab.io/attribute_tree)
and [readthedocs](http://attribute-tree.readthedocs.io)

`attribute_tree` wraps dict into tree-like structure with value access via getattr.

# install

```
mkdir attribute_tree
wget -qO- https://gitlab.com/attu/attribute_tree/-/archive/VERSION/attribute_tree-VERSION.tar.gz | tar xvz -C attribure_tree/ --strip-components 1
pip install ./attribute_tree/
```

# examples

basic attribute setting

``` python
from attribute_tree import attribute_tree

# create tree attributes like dict items
tree = attribute_tree(lorem='ipsum')
assert tree.lorem == 'ipsum'

# add new attributes to the root of tree
tree.dolor = 42
assert tree.dolor == 42

# add nested attributes
tree.consectetur.adipiscing = (-1.0 / 12)
assert isclose(tree.consectetur.adipiscing, -0.08333333333333333)

# add list of nested attributes
tree.elit = [attribute_tree(sed='eiusmod'), attribute_tree(tempor='incididunt')]
assert tree.elit[0].sed == 'eiusmod'
assert tree.elit[1].tempor == 'incididunt'
```

converting tree to dictionary

``` python
from attribute_tree import attribute_tree

tree = attribute_tree(lorem='ipsum')
tree.dolor.sit = 'amet'

assert vars(tree) == {'lorem': 'ipsum', 'dolor': {'sit': 'amet'}}
```

nested trees

``` python
from attribute_tree import attribute_tree

tree = attribute_tree(lorem=dict(ipsum='dolor'))

assert tree.lorem.ipsum == 'dolor'

tree.sit = [dict(amet='consectetur')]
assert tree.sit[0].amet == 'consectetur'
```
